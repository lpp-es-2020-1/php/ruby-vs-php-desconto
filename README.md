## Documentação

Documentação do desafio: [Ruby vs PHP. Desconto](https://turing.inf.ufg.br/mod/forum/discuss.php?d=1869)

## Descrição do problema
### Desconto
Uma loja está tendo uma promoção onde você compra 3 produtos e o mais barato sai de graça, mas por motivos legais eles não podem cobrar 0 reais por um produto, então o desconto é aplicado sobre a compra toda.

Por exemplo se alguém compra os produtos ‘a’,’b’ e ‘c’, onde ‘a’ vale 50 reais, ‘b’ vale 24.99 e ‘c’ vale 32.60, o produto ‘b’ sai de graça, por tanto o valor total é ‘a’+’c’ que é 82.60 mas na nota fiscal fica:

produto a 50.00 - desconto = 41.67

produto b 24.99 - desconto = 16.66

produto c 32.60 - desconto = 24.27

total a pagar = 82.60

O terceiro produto é descontado mas como ele não pode custar 0 reais o desconto é dividido entre todos os produtos.

Crie uma função que receba um array com o preço de três produtos e retorne os preços com desconto.

Obs.: Se a pessoa comprar 6 produtos os 2 produtos mais baratos saem de graça, e o resultado deve ter duas casas decimais já que estamos falando de dinheiro.

**Exemplos:**

desconto([<array com os preços dos produtos>])

 retorna=> [<array com os preços depois do desconto>]


desconto([32.55, 9.99])

retorna=> [32.55, 9.99] <como o array só tem dois produtos nem um desconto é aplicado>


desconto([99.50, 85.20, 70.70])

retorna=> [75.93, 61.63, 47.13]


desconto([99.50, 85.20, 70.70, 50.50, 66.20, 55.20, 99.30]) 

retorna=> [84.4, 70.1, 55.6, 35.4, 51.1, 40.1, 84.2]

# Instalação

Execute os seguintes comandos para executar o código via Docker

```bash
docker build -t ruby-vs-php-1 .
docker run -it --rm --name ruby-vs-php-1 ruby-vs-php-1
```