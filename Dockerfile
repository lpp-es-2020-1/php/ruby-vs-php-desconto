FROM php:7.4-cli
COPY . /usr/src/ruby-vs-php-1
WORKDIR /usr/src/ruby-vs-php-1
CMD [ "php", "./index.php" ]