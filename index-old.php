<?php

// Valor dos produtos comprados
$valores_produtos = [50, 10, 30, 20, 40];

// Quantos produtos foram comprados
$qtde_produtos = count($valores_produtos);

// Obter quantos produtos devem ser descontados
$qtde_produtos_descontados = $qtde_produtos % 3;

// Ordenar o array para que os primeiros sejam os mais baratos
sort($valores_produtos);

// Pegar os produtos que devem ser descontados
$produtos_descontados = array_slice($valores_produtos, 0, $qtde_produtos_descontados);

// Somar os valores dos produtos descontados
$total_desconto = array_sum($produtos_descontados);

// Dividir o desconto total entre os produtos (entrada)
foreach ($valores_produtos as &$valor) {
	$valor = $valor - ($total_desconto / $qtde_produtos);
	$valor = round($valor, 2); // arredondar para 2 casas decimais (R$)
}

// Imprimir na tela os novos valores
echo json_encode($valores_produtos);

?>