<?php

function desconto($valoresProdutos) {

    # Cria um array ordenado dos produtos
    sort($valoresProdutos);

    # Inicializa váriaveiis
    $prodEmDesc = [];
    $desc = 0;
    $valoresProdutosRtrn = [];

    # Separa os produtos que serão descontados
    $prodEmDesc = array_slice($valoresProdutos, 0, count($valoresProdutos)/3);

    # Soma o valor total do desconto;
    $desc = array_sum($prodEmDesc);

    # Divide o valor total do desconto entre todos os produtos, arredonda e coloca no array que irá retornar
    $valoresProdutosRtrn = array_map(function($valor) use ($desc, $valoresProdutos) {
        return round($valor - ($desc / count($valoresProdutos)), 2);
    }, $valoresProdutos);

    return $valoresProdutosRtrn;
} 

# Ler arquivo em disco
$tabela = fopen("array_gen.csv", "r");
$i = 1;

if ($tabela !== false) {
    
    while (true) {

        // Ler linha a linha do arquivo
        $linha = fgetcsv($tabela);

        // Leitura chegou no final, encerrar laço de repetição
        if ($tabela === false) {
            break;
            
        } else {

            // Enquanto a leitura for um array, executar método para aplicar o desconto
            if (is_array($linha)) {

                $descontoAplicado = desconto($linha);
                echo "Array #${i}: ".json_encode($descontoAplicado)."\n";
            
            // Caso contrário, encerrar laço de repetição
            } else {
                break;
            }
        }

        $i++;
    }

    // Fechar leitura do arquivo em disco
    fclose($tabela);
}

?>